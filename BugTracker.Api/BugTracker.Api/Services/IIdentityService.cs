﻿using BugTracker.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BugTracker.Services
{
    public interface IIdentityService
    {
        Task<User> GetUser(string id);
        Task<IEnumerable<User>> GetUsers();
    }
}
