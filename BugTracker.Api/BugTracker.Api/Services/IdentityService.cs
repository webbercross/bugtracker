﻿using BugTracker.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Services
{
    public class IdentityService : IIdentityService
    {
        private IConfiguration _configuration;

        public IdentityService(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }

        public async Task<User> GetUser(string id)
        {
            var token = await GetApiToken();

            var auth0Client = new HttpClient();
            auth0Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await auth0Client.GetAsync($"{_configuration["Auth0:Authority"]}api/v2/users/{id}");

            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<User>(responseString);

                return user;
            }

            return null;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var token = await GetApiToken();

            var auth0Client = new HttpClient();
            auth0Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await auth0Client.GetAsync($"{_configuration["Auth0:Authority"]}api/v2/users");

            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();

                var users = JsonConvert.DeserializeObject<IEnumerable<User>>(responseString);

                return users;
            }

            return null;
        }

        async Task<string> GetApiToken()
        {
            var auth0Client = new HttpClient();
            string token = "";
            var bodyString = $@"{{""client_id"":""{_configuration["Auth0:ApiClientId"]}"", ""client_secret"":""{_configuration["Auth0:ApiClientSecret"]}"", ""audience"":""{_configuration["Auth0:ApiAudience"]}"", ""grant_type"":""client_credentials""}}";
            var response = await auth0Client.PostAsync($"{_configuration["Auth0:ApiAuthority"]}oauth/token", new StringContent(bodyString, Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                var responseJson = JObject.Parse(responseString);
                token = (string)responseJson["access_token"];
            }

            return token;
        }
    }
}
