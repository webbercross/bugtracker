﻿using BugTracker.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugTracker.Data
{
    public class BugTrackerContext : DbContext
    {
        public BugTrackerContext(DbContextOptions<BugTrackerContext> options)
            : base(options)
        {

        }

        public DbSet<Bug> Bugs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bug>()
                .HasKey(k => k.Id);

            base.OnModelCreating(modelBuilder);
        }

        public async Task EnsureSeedData()
        {
            if (await Bugs.AnyAsync())
                return;

            await Bugs.AddRangeAsync(
                new Bug
                {
                    Title = "Test bug 1",
                    Details = "This is a test bug to show the API works"
                },
                new Bug
                {
                    Title = "Test bug 1",
                    Details = "This is a test bug to show the API works"
                });

            await SaveChangesAsync();
        }
    }
}
