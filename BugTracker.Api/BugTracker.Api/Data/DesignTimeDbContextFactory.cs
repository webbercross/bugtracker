﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.IO;

namespace BugTracker.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BugTrackerContext>
    {
        public BugTrackerContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                //.SetBasePath(Path.GetFullPath(@"../../.."))
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<BugTrackerContext>();
            builder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            return new BugTrackerContext(builder.Options);
        }
    }
}
