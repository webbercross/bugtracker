﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BugTracker.Migrations
{
    public partial class UserNamesAndIsFixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignedUserName",
                table: "Bugs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedUserName",
                table: "Bugs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFixed",
                table: "Bugs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedUserName",
                table: "Bugs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedUserName",
                table: "Bugs");

            migrationBuilder.DropColumn(
                name: "CreatedUserName",
                table: "Bugs");

            migrationBuilder.DropColumn(
                name: "IsFixed",
                table: "Bugs");

            migrationBuilder.DropColumn(
                name: "UpdatedUserName",
                table: "Bugs");
        }
    }
}
