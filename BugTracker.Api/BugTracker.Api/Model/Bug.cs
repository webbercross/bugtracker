﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugTracker.Model
{
    public class Bug
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUserId { get; set; }
        public string CreatedUserName { get; set; }

        public DateTime UpdatedDate { get; set; }
        public string UpdatedUserId { get; set; }
        public string UpdatedUserName { get; set; }

        public string AssignedUserId { get; set; }
        public string AssignedUserName { get; set; }

        public bool IsFixed { get; set; }
    }
}
