﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BugTracker.Data;
using BugTracker.Model;
using BugTracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BugTracker.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Bug")]
    public class BugController : Controller
    {
        readonly BugTrackerContext _ctx;
        readonly IIdentityService _identityService;

        public BugController(BugTrackerContext ctx,
            IIdentityService identityService)
        {
            _identityService = identityService;
            _ctx = ctx;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> Get()
        {
            var bugs = await _ctx.Bugs.Where(b => !b.IsFixed)
                .OrderByDescending(b => b.UpdatedDate)
                .ToListAsync();

            return Ok(bugs);
        }

        [HttpGet, Authorize]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var bug = await _ctx.Bugs.FirstOrDefaultAsync(b => b.Id == id);

            return Ok(bug);
        }

        [HttpPost, Authorize]
        [Route("")]
        public async Task<IActionResult> Post([FromBody] Bug bug)
        {
            var userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var user = await _identityService.GetUser(userId);
            var assignedUser = await _identityService.GetUser(bug.AssignedUserId);

            bug.CreatedDate = DateTime.UtcNow;
            bug.CreatedUserId = userId;
            bug.CreatedUserName = user.Name;

            bug.UpdatedDate = DateTime.UtcNow;
            bug.UpdatedUserId = userId;
            bug.UpdatedUserName = user.Name;

            bug.AssignedUserName = assignedUser.Name;

            await _ctx.Bugs.AddAsync(bug);

            await _ctx.SaveChangesAsync();

            return Ok();
        }

        [HttpPatch, Authorize]
        [Route("")]
        public async Task<IActionResult> Patch([FromBody] Bug bug)
        {
            if (bug == null)
                return BadRequest();

            var existing = await _ctx.Bugs.FindAsync(bug.Id);

            if (existing == null)
                return NotFound();

            var userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            var user = await _identityService.GetUser(userId);
            var assignedUser = await _identityService.GetUser(bug.AssignedUserId);

            existing.AssignedUserId = bug.AssignedUserId;
            existing.AssignedUserName = bug.AssignedUserName;

            existing.Title = bug.Title;
            existing.Details = bug.Details;
            existing.IsFixed = bug.IsFixed;

            existing.UpdatedDate = DateTime.UtcNow;
            existing.UpdatedUserId = userId;
            existing.CreatedUserName = user.Name;

            existing.AssignedUserName = assignedUser.Name;

            await _ctx.SaveChangesAsync();

            return Ok();
        }
    }
}