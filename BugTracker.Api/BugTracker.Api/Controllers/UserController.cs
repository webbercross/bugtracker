﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BugTracker.Model;
using BugTracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BugTracker.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        readonly IConfiguration _configuration;
        readonly IIdentityService _identityService;

        public UserController(IConfiguration Configuration,
            IIdentityService identityService)
        {
            _configuration = Configuration;
            _identityService = identityService;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> Get()
        {
            var users = await _identityService.GetUsers();

            return Ok(users);
        }

        [HttpGet, Authorize]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var user = await _identityService.GetUser(id);

            return Ok(user);
        }
    }
}