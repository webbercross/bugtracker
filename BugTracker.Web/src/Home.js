import React from 'react';
import {Link} from 'react-router-dom';
import './Home.css'

class Home extends React.Component {
  constructor() {
      super();
      this.state = {bugList: []};
  }

  componentDidMount() {
    const accessToken = this.props.auth.getAccessToken();
    //console.log(accessToken);
    const apiUrl = "https://webbercrossbugtracker-api.azurewebsites.net";
    
    fetch(apiUrl + "/api/bug", {headers: new Headers({
        "Accept": "application/json",
        "Authorization": `Bearer ${accessToken}`
          })
        })
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          if (response.status === 403) {
            alert("You are not authorized!")
          }          
        })
        .then(bugs => this.setState({bugList: bugs || []}))
        .catch(error => console.log(error))
  }

  render() {
    const bugList = this.state.bugList.map((bug) => 
    <li key={bug.id}>
      <Link to={"/bugForm/" + bug.id}><h3>{bug.id} [{new Date(bug.createdDate).toLocaleDateString()} {new Date(bug.createdDate).toLocaleTimeString()}] {bug.title}</h3></Link>
      <p className="created">Created: {new Date(bug.createdDate).toLocaleDateString()} {new Date(bug.createdDate).toLocaleTimeString()} by {bug.createdUserName}</p>
      <p className="updated">Updated: {new Date(bug.updatedDate).toLocaleDateString()} {new Date(bug.updatedDate).toLocaleTimeString()} by {bug.updatedUserName}</p>
      <p className="assigned">Assigned to: {bug.assignedUserName}</p>
      <p className="details">{bug.details}</p>
    </li>);

    const addBugButton = <Link className="bugLink" to="/bugForm">Add a bug</Link>;

    return  <div>
              {addBugButton}
              <ul>
                {bugList}
              </ul>
            </div>;
  }

}

export default Home;

