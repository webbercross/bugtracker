import React from 'react';
import './BugForm.css'

class BugForm extends React.Component {

  apiUrl = "https://webbercrossbugtracker-api.azurewebsites.net";

  constructor() {
    super();
    this.state = {
        id: 0,
        details: "",
        title: "",
        assignedUserId: "",
        isFixed: false
      };
    this.userList = [];
  }

  handleDetailsChange(e) {
    this.setState({details: e.target.value});
  }

  handleTitleChange(e) {
    this.setState({title: e.target.value});
  }

  handleAssignedUserChange(e) {
    this.setState({assignedUserId: e.target.value});
  }

  handleIsFixedChange(e) {
    this.setState({isFixed: e.target.checked});
  }

  loadBug() {

    const accessToken = this.props.auth.getAccessToken();
    var id = this.props.match.params.id;

   fetch(this.apiUrl + "/api/bug/" + id, {headers: new Headers({
      "Accept": "application/json",
      "Authorization": `Bearer ${accessToken}`
        })
      })
      .then(response => {
        if (response.ok) {
          console.log(response);
          return response.json()
        }
        if (response.status === 403) {
          alert("You are not authorized!")
        }          
      })
      .then(bug =>
        {
          console.log(bug);
          this.setState({id: bug.id || 0});
          this.setState({details: bug.details || ""});
          this.setState({title: bug.title || ""});
          this.setState({assignedUserId: bug.assignedUserId || ""});
        })
      .catch(error => console.log(error))
  }

  componentDidMount() {

    var id = this.props.match.params.id;

    const accessToken = this.props.auth.getAccessToken();
    
    fetch(this.apiUrl + "/api/user", {headers: new Headers({
        "Accept": "application/json",
        "Authorization": `Bearer ${accessToken}`
          })
        })
        .then(response => {
          if (response.ok) {
            return response.json()
          }
          if (response.status === 403) {
            alert("You are not authorized!")
          }          
        })
        .then(users => {
            this.userList = users || [];
            this.setState({assignedUserId: users[0].user_id || ""});

            if(id)
              this.loadBug();
          })
        
        //.then(users => console.log(users))
        .catch(error => console.log(error))
  }

  render() {

    var id = this.props.match.params.id;

    const userSelect = this.userList.map((user, i) => 
    <option key={user.user_id} value={user.user_id}>{user.name}</option>);
    const isFixed = id > 0 ? <div className="row">
      <label className="col-50" htmlFor="details">Is Fixed</label>
      <div><input type="checkbox" name="isFixed" checked={this.state.isFixed} onChange={(e)=> this.handleIsFixedChange(e)}/></div>
      </div> : null;

    return <div className="formContainer">
    <form onSubmit={(e) => this.handleFormSubmit(e)}>
        <div className="row">
            <label className="col-50" htmlFor="title">Title</label>
            <input required="required" type="text" name="title" value={this.state.title} onChange={(e)=> this.handleTitleChange(e)}/>
        </div>
        <div className="row">
            <label className="col-50" htmlFor="details">Details</label>
            <textarea required="required" rows="10" name="details" value={this.state.details} onChange={(e)=> this.handleDetailsChange(e)}></textarea>
        </div>
        <div className="row">
            <label className="col-50" htmlFor="details">Assigned to</label>
            <select required="required" name="assignedUserId" value={this.state.assignedUserId} onChange={(e)=> this.handleAssignedUserChange(e)}>
            {userSelect}
            </select>
        </div>
        {isFixed}
        <div className="row">
            <input type="submit" value="Submit bug"/>
        </div>
    </form>
    </div>;
  }

  handleFormSubmit(e) {
    e.preventDefault();
    const accessToken = this.props.auth.getAccessToken();
    const apiUrl = "https://webbercrossbugtracker-api.azurewebsites.net";

    var id = this.props.match.params.id;

    fetch(apiUrl + "/api/bug", {
      method: id > 0 ? "PATCH" : "POST",
      body: JSON.stringify(this.state),
      headers: new Headers({
        "Content-Type": "application/json",
        "Authorization": `Bearer ${accessToken}`
    }),
      credentials: "include",
      mode: "cors",
    }).then((response) => {
      if (response.ok) {
        this.props.history.push("/");
      } else {
        alert(response.statusText);
      }
    });

    console.log("submitting " + JSON.stringify(this.state));
  }
}

export default BugForm;
