export const AUTH_CONFIG = {
    domain: 'webbercross.eu.auth0.com',
    clientID: 'gso3UuIbVveZP8SnpqdyRdQI0zmiCSoE',
    redirectUri: 'https://webbercrossbugtracker-web.azurewebsites.net/startSession',
    //redirectUri: 'http://localhost:3000/startSession',
    audience: 'https://webbercrossbugtracker-api.azurewebsites.net'
}