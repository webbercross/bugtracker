# BugTracker Demo app

In order to build a modern app with relevant technologies, I decided to use the following:

* [ASP.Net Core 2](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1) for the web API
* [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) for creating the database using code-first migrations
* [ReactJS](https://reactjs.org/) for the SPA website
* [Auth0](https://auth0.com/) for identity management
* [Azure Web Apps](https://azure.microsoft.com/en-gb/services/app-service/web/) for hosting the website and API so the website can be demonstrated
* [Azure SQL Database](https://azure.microsoft.com/en-gb/services/sql-database/) for hosting the database, so the website so it can be demonstrated

This project was based on this [Auth0 react demo app](https://auth0.com/blog/developing-web-apps-with-asp-dot-net-core-2-dot-0-and-react-part-1/).
I used it as a starting point as it had good examples of using Auth0 with React and Asp.Net Core

## [Web app demo](https://webbercrossbugtracker-web.azurewebsites.net/)
The app is hosted [here](https://webbercrossbugtracker-web.azurewebsites.net/)
Please register and try it out!
The API is hosted [here](https://webbercrossbugtracker-api.azurewebsites.net/)

## Building the app
I've beening meaning to look at using Auth0 for managing user authentication and this looked like a good opportunity to do that. It's a cloud-based auth provider with support for signing up with a user name and password or an external provider like Google. There is a comprehensive API and fine-grained access control and scoping.
I use ASP.Net core and EF core on a day-to-day basis, so this was an obvious choice to use.
I've never used react before, so this was a good experience. The create-react-app module which the example project is built from creates a fully buildable and runable app out-of-the-box so it's a great starting point.

## Areas of improvement
* Auth0 claims should be configurable to include user name, however after quite a while trying different things, I couldn't get it to work, so I ended up pulling user info from the API instead.
* The API is all in one project and could do with separating into other projects to make it more maintainable.
* I didn't have time to spend on styling, so it's very basic!

## Tools used
I used the following tools
* Visual Studio 2017 to build the API
* Visual Studio Code to work on the Website
* npm to run react and build the website